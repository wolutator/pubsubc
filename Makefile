#  Makefile - A simple client for MQTT in C
#  Wolfgang Hottgenroth <woho@hottis.de>
#  https://home.hottis.de/gitlab/wolutator/pubsubc



CFLAGS?=-mcpu=cortex-m3 -mthumb -Og -fdata-sections -ffunction-sections -g -gdwarf-2 -Wall -Werror

CC=arm-none-eabi-gcc
AR=arm-none-eabi-ar

CFLAGS+=-I../ioLibrary_Driver/Ethernet -Isrc

OBJDIR=build
VPATH=src

OBJS=$(addprefix $(OBJDIR)/,pubsubc.o platformAdaption.o)

all:	$(OBJS)
	$(AR) rcs pubsubc.a $^

$(OBJDIR)/%.o:	%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJS): | $(OBJDIR)

$(OBJDIR):
	mkdir $(OBJDIR)

.PHONY: clean
clean:
	-rm -rf $(OBJDIR) pubsubc.a
