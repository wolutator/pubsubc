# pubsubc 

This is a minimal MQTT client library in C.

It comes with an adaption layer for the ioLibrary_Driver for the
WizNet chips which can be found here https://github.com/Wiznet/ioLibrary_Driver.

You find a fork with a Makefile a STMCubeMX generated project here
https://home.hottis.de/gitlab/wolutator/ioLibrary_Driver/-/tree/WolfgangsOwnBranch.

Using this adaption layer you should find it easy to adjust it for
other platforms.

This work is directly derived from the famous PubSubClient library
of Nick O'Leary, which can be found at https://pubsubclient.knolleary.net/

It is most or less a plain C rewrite.

All honour for this working MQTT client library goes to Nick O'Leary,
all blame for bugs in the C port shall go to me.


## API


The library provides the following functions:


### Initialization

    void mqttClientInit(mqttClient_t *mqttClient, client_t *client, callback_t callback);

Initializes the MQTT client, handover a client handle according to the definition
from ``platformAdaption.h`` and a callback function for incoming messages.

Implement this callback function for incoming messages with this footprint:

    typedef void (*callback_t)(char*, uint8_t*, uint16_t);

### Connect

    bool mqttConnect(mqttClient_t *mqttClient, 
             uint8_t *address, uint16_t port,
             const char *id,
             const char *user, const char *pass,
             const char *willTopic, uint8_t willQos,
             bool willRetain, const char *willMessage,
             bool cleanSession);

In the original C++ implementation multiple variants of the ``connect``method are available 
with less and lesser arguments. Unfortunately, in C this is no option. If you don't care
about authentication and the whole will stuff you can call it

    mqttConnect(&mqttClient, brokerAddress, brokerPort, clientId, NULL, NULL, NULL, 0, false, NULL, false);

### Disconnect

    void mqttDisconnect(mqttClient_t *mqttClient);

### Publish

    bool publish(mqttClient_t *mqttClient,
             const char *topic, 
             const uint8_t *payload, uint16_t plength,
             bool retained);

### Subscribe and Unsubscribe

    bool subscribe(mqttClient_t *mqttClient,
               const char *topic, uint8_t qos);
    
    bool unsubscribe(mqttClient_t *mqttClient,
                 const char* topic);

### Loop

    bool mqttLoop(mqttClient_t *mqttClient);

Call this function in your idle loop. For my own part, I've implemented a time-triggered system
and call it with a period of 100ms.

### Connection status

    bool mqttConnected(mqttClient_t *mqttClient);

