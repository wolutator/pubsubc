/*
  pubsubc.h - A simple client for MQTT in C
  Derived from the PubSubClient from Nick O'Leary
  Wolfgang Hottgenroth <woho@hottis.de>
  https://home.hottis.de/gitlab/wolutator/pubsubc
*/



#ifndef _PUBSUBC_H_
#define _PUBSUBC_H_


#define MQTT_VERSION_3_1      3
#define MQTT_VERSION_3_1_1    4

// MQTT_VERSION : Pick the version
//#define MQTT_VERSION MQTT_VERSION_3_1
#ifndef MQTT_VERSION
#define MQTT_VERSION MQTT_VERSION_3_1_1
#endif

// MQTT_MAX_PACKET_SIZE : Maximum packet size. Override with setBufferSize().
#ifndef MQTT_MAX_PACKET_SIZE
#define MQTT_MAX_PACKET_SIZE 512
#endif

// MQTT_KEEPALIVE : keepAlive interval in Seconds. Override with setKeepAlive()
#ifndef MQTT_KEEPALIVE
#define MQTT_KEEPALIVE 15
#endif

// MQTT_SOCKET_TIMEOUT: socket timeout interval in Seconds. Override with setSocketTimeout()
#ifndef MQTT_SOCKET_TIMEOUT
#define MQTT_SOCKET_TIMEOUT 15
#endif




// Possible values for client.state()
#define MQTT_CONNECTION_TIMEOUT     -4
#define MQTT_CONNECTION_LOST        -3
#define MQTT_CONNECT_FAILED         -2
#define MQTT_DISCONNECTED           -1
#define MQTT_CONNECTED               0
#define MQTT_CONNECT_BAD_PROTOCOL    1
#define MQTT_CONNECT_BAD_CLIENT_ID   2
#define MQTT_CONNECT_UNAVAILABLE     3
#define MQTT_CONNECT_BAD_CREDENTIALS 4
#define MQTT_CONNECT_UNAUTHORIZED    5

#define MQTTCONNECT     1 << 4  // Client request to connect to Server
#define MQTTCONNACK     2 << 4  // Connect Acknowledgment
#define MQTTPUBLISH     3 << 4  // Publish message
#define MQTTPUBACK      4 << 4  // Publish Acknowledgment
#define MQTTPUBREC      5 << 4  // Publish Received (assured delivery part 1)
#define MQTTPUBREL      6 << 4  // Publish Release (assured delivery part 2)
#define MQTTPUBCOMP     7 << 4  // Publish Complete (assured delivery part 3)
#define MQTTSUBSCRIBE   8 << 4  // Client Subscribe request
#define MQTTSUBACK      9 << 4  // Subscribe Acknowledgment
#define MQTTUNSUBSCRIBE 10 << 4 // Client Unsubscribe request
#define MQTTUNSUBACK    11 << 4 // Unsubscribe Acknowledgment
#define MQTTPINGREQ     12 << 4 // PING Request
#define MQTTPINGRESP    13 << 4 // PING Response
#define MQTTDISCONNECT  14 << 4 // Client is Disconnecting
#define MQTTReserved    15 << 4 // Reserved

#define MQTTQOS0        (0 << 1)
#define MQTTQOS1        (1 << 1)
#define MQTTQOS2        (2 << 1)

// Maximum size of fixed header and variable length size header
#define MQTT_MAX_HEADER_SIZE 5


#include <stdint.h>
#include <stdbool.h>

#include <platformAdaption.h>


typedef void (*callback_t)(char*, uint8_t*, uint16_t);

typedef struct {
    client_t *client;
    uint8_t buffer[MQTT_MAX_PACKET_SIZE];
    uint16_t bufferSize;
    uint16_t keepAlive;
    uint16_t socketTimeout;
    uint16_t nextMsgId;
    uint32_t lastOutActivity;
    uint32_t lastInActivity;
    bool pingOutstanding;
    callback_t callback;
    uint8_t *brokerAddress;
    uint16_t brokerPort;
    int state;
} mqttClient_t;


void mqttClientInit(mqttClient_t *mqttClient, client_t *client, callback_t callback);
bool mqttConnect(mqttClient_t *mqttClient, 
             uint8_t *address, uint16_t port,
             const char *id,
             const char *user, const char *pass,
             const char *willTopic, uint8_t willQos,
             bool willRetain, const char *willMessage,
             bool cleanSession);

void mqttDisconnect(mqttClient_t *mqttClient);

bool publish(mqttClient_t *mqttClient,
             const char *topic, 
             const uint8_t *payload, uint16_t plength,
             bool retained);

bool subscribe(mqttClient_t *mqttClient,
               const char *topic, uint8_t qos);
bool unsubscribe(mqttClient_t *mqttClient,
                 const char* topic);

bool mqttLoop(mqttClient_t *mqttClient);

bool mqttConnected(mqttClient_t *mqttClient);

#endif // _PUBSUBC_H_
