/*
  platformAdaption.h - A simple client for MQTT in C
  Wolfgang Hottgenroth <woho@hottis.de>
  https://home.hottis.de/gitlab/wolutator/pubsubc
*/


#ifndef _PLATFORMADAPTION_H_
#define _PLATFORMADAPTION_H_


#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>


#define PSC_SUCCESS 1
#define PSC_TIMED_OUT -1
#define PSC_INVALID_SERVER -2
#define PSC_TRUNCATED -3
#define PSC_INVALID_RESPONSE -4

#define PSC_TIMEOUT_MS 1000


typedef struct {
    uint8_t sockNum;
} client_t;

int clientConnect(client_t *client, uint8_t *host, uint16_t port);
int clientAvailable(client_t *client);
void clientStop(client_t *client);
int clientRead(client_t *client);
size_t clientWrite(client_t *client, const uint8_t *buf, size_t size);
size_t clientWriteOne(client_t *client, uint8_t b);
void clientFlush(client_t *client);
bool clientConnected(client_t *client);

uint32_t millis();

uint32_t HAL_GetTick(void);
int logMsg(const char *format, ...);

#endif // _PLATFORMADAPTION_H_