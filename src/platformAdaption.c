/*
  platformAdaption.c - A simple client for MQTT in C
  Wolfgang Hottgenroth <woho@hottis.de>
  https://home.hottis.de/gitlab/wolutator/pubsubc
*/


#include <platformAdaption.h>
#include <socket.h>

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>


int clientConnect(client_t *client, uint8_t *host, uint16_t port) {
  int8_t res = socket(client->sockNum, Sn_MR_TCP, port, SF_IO_NONBLOCK);
  if (res != client->sockNum) {
    close(client->sockNum);
    return PSC_INVALID_RESPONSE;
  }
  logMsg("clientConnect: socket initialized");

  res = connect(client->sockNum, host, port);
  if (res != SOCK_BUSY) {
    close(client->sockNum);
    return PSC_INVALID_RESPONSE;
  }
  uint32_t startTime = HAL_GetTick();
  while (startTime + PSC_TIMEOUT_MS > HAL_GetTick()) {
    uint8_t sockState = getSn_SR(client->sockNum);
    if (sockState == SOCK_ESTABLISHED) {
      logMsg("clientConnect: connection established");
      return PSC_SUCCESS;
    }
  }
  return PSC_TIMED_OUT;
}

int clientAvailable(client_t *client) {
  return getSn_RX_RSR(client->sockNum);
}

void clientStop(client_t *client) {
  int8_t res = disconnect(client->sockNum);

  if (res != SOCK_BUSY) {
    close(client->sockNum);
    logMsg("clientStop: disconnect returns %d, invalid response, ignore it", res);
  } else {
    bool successfullyClosed = false;
    uint32_t startTime = HAL_GetTick();
    while (startTime + PSC_TIMEOUT_MS > HAL_GetTick()) {
      uint8_t sockState = getSn_SR(client->sockNum);
      if (sockState == SOCK_CLOSED) {
        logMsg("clientStop: connection closed");
        successfullyClosed = true;
        break;
      }
    }
    if (successfullyClosed) {
      logMsg("clientStop: done");
      close(client->sockNum);
    } else {
      logMsg("clientStop: timeout when closing, ignore");
      close(client->sockNum);
    }
  }
}

int clientRead(client_t *client) {
  int res = -1;
  if (clientAvailable(client) >= 1) {
    uint8_t buf;
    res = recv(client->sockNum, &buf, 1);
    if (res == 1) {
      res = (int) buf;
    }
  }
  return res;
}

size_t clientWrite(client_t *client, const uint8_t *buf, size_t size) {
    int32_t res = send(client->sockNum, (uint8_t*) buf, size);
    return (res == size) ? size : 0;
}

size_t clientWriteOne(client_t *client, uint8_t b) {
  return clientWrite(client, &b, 1);
}

void clientFlush(client_t *client) {
  // does nothing
}

bool clientConnected(client_t *client) {
  return (getSn_SR(client->sockNum) == SOCK_ESTABLISHED) ? 1 : 0;
}

uint32_t millis() {
  return HAL_GetTick();
}


